# New-Tab

My simple home-page dashboard. It's just a fancy list of links for now.

# Getting up and running

You can just preview `index.html` in a browser. It runs over `file://` as well as `http://`.

However, the actual data file has not been pushed to the repo, as it contains sensitive information.
A sample data file has been included instead, so you can at least see how it works. Ensure that you load
`data-sample.js` (instead of `data.min.js`) at the bottom of `index.html`.

It also includes a `manifest.json` file, which allows it to act as a "new tab" application
in Google Chrome. Just load this directory as an extension in Chrome, and all new tabs
will load the dashboard.

# How it's built

The framework is AngularJS, and the colors are from Material.

Since this was built for personal use, the font is loaded locally rather
than from a CDN. If you don't have Roboto installed, feel free to change
it to any font of your choosing.

The page is not responsive, because it's only meant to be running on my development machine.