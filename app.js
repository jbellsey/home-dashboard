
// yes, even a tiny app gets an IIFE for
// namespace cleanliness
//

(function() {

    // injection list not needed, as I'm not uglifying these JS files (just minifying them)
    //
    var HomeController = function($timeout, HomeDataService) {

        var self = this;

        // delay the installation of the data. this forces
        // the intro animation to run (but not stagger)
        //
        $timeout(function(){
            self.data = HomeDataService.data;
        },1);

        this.data = [];
        this.visibleSection = 0;    // set to 0 (first section open by default) or -1 (nothing open to start)
        this.sectionClicked = function(ix) {
            self.visibleSection = ix;
        }
    };

    //--------------

    angular.module('home',['ngAnimate','ngSanitize'])
        .controller('HomeController', HomeController);

})();