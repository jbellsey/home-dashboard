/*
 * This file contains sample data. The real file
 * has sensitive data. To use, just change the script
 * tag in "index.html" to load this file.
 */
(function() {

	function HomeDataService() {

		var resources = [
            {
                title: "YYYYYYYY",
                cls: 'home-orange',
                links: [
                    [
                        //-----------
                        { subsection: 'ZZZZZZ ZZZZZ'},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXXXXXXXXXXXX'}
                    ],

                    //-----------
                    [
                        { subsection: 'ZZZZZZ ZZZZZZZZZZZZ'},
                        { multi: [
                            { url: 'http://cnn.com',
                                text: 'XXXXXXXXXXXX XXXX'},
                            { url: 'http://cnn.com',
                                text: 'XXXXXX XXXXXXXXXXX'}
                        ]}
                    ],

                    //-----------
                    [
                        { subsection: 'ZZZZZZ ZZ'},
                        { multi: [
                            { url: 'http://cnn.com',
                                text: 'XXXXXXXXXXXXXXXXX'},
                            { url: 'http://cnn.com',
                                text: 'XXXXX'}
                        ]},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXXXXXXXXXXXX XXX'}
                    ],

                    //-----------
                    [
                        { subsection: 'ZZZZZZZZZ ZZZZZ'},
                        { url: 'http://cnn.com',
                            text: 'XXXXXX XXXXXXXXXXXXXXXXX XXX'},
                        { multi: [
                            { url: 'http://cnn.com',
                                text: 'XXXX XXX XXXXX'},
                            { url: 'http://cnn.com',
                                text: 'XXXXXXXXX XXXXX'}
                        ]},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXX XXXXX' }
                    ]

                ]
            },
            //=====================================================
            //=====================================================
            {
                title: "YYY YYYYYYY",
                cls: 'home-blue',
                links: [
                    [
                        { url: 'http://cnn.com',  text: 'XXXXXXXXXX'},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXX XXX XXXXXXXXX'}
                    ],

                    [
                        //---------- DEBUGGING
                        { subsection: 'ZZZZZ ZZZZZZZ'},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXX XX XXXXXXX'},
                    ],

                    [
                        //---------- DOCS
                        { subsection: 'ZZZZ'},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXXX'},
                        { url: 'http://cnn.com',
                            text: 'XXXX XXXXXXXXX XXXXX'},
                        { sep: '---------'},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXX'},
                        { url: 'http://cnn.com',
                            text: "XXXXX XXXXX" },
                        { url: 'http://cnn.com',
                            text: 'XXX X XXXXXXXX'},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXX XXX XXXXX'},
                        { url: 'http://cnn.com/', text: "XXXX XXXX"},
                        { url: 'http://cnn.com/', text: 'XXXXXX XXXX XXXXX' },
                        { url: 'http://cnn.com',
                            text: 'XXXXX'},
                        { url: 'http://cnn.com',
                            text: 'XXXXX XXXXXXXXXX' },
                        { url: 'http://cnn.com', text: "XXXXX XXXXX" }
                    ]
                ]
            },
            {
                title: 'YYYYY',
                cls: 'home-pink',
                links: [
                    [
                        { subsection: 'ZZZZZZZZZZ ZZZZ'},
                        { url: 'http://cnn.com',
                            text: 'XXXXX'},
                        { url: 'http://cnn.com',
                            text: 'XXXX XX'},
                        { url: 'http://cnn.com',
                            text: 'XXXXXXXXX' },
                        { url: 'http://cnn.com',
                            text: 'XXXXXXXX XXXXXXX'},
                        { url: 'http://cnn.com/',
                            text: 'XX XXXXXXXX'}
                    ],

                    [
                        { subsection: "ZZZZZZZZ" },
                        { url: 'http://cnn.com', text: "XXXXX XXXXX"},
                        { url: 'http://cnn.com', text: "XXX XXXXXXXX"},
                        { url: 'http://cnn.com', text: "XXXXX XXX XXXXX"},
                        { url: 'http://cnn.com', text: 'XXX XXXXXXXXX'}
                    ]
                ]
            },
            {
                title: "YYYYYY YYYYYYY",
                cls: 'home-purple',
                links: [
                    [
                        { url: 'http://cnn.com/', text: 'XXXXX XXX XXXXX'},
                        { url: 'http://cnn.com/', text: 'XXX XXXXXXX XXX'},
                        { url: 'http://cnn.com/', text: "XXXXXXXXXX"},
                        { url: 'http://cnn.com/', text: 'XXXX XXXXX'}
                    ]
                ]
            },
            {
                title: "YYY YYYYYYYY YY",
                cls: 'home-bluegray',
                links: [
                    [
                        { url: 'http://cnn.com/', text: 'XXXXX'},
                        { url: 'http://cnn.com/', text: 'XXX XXXXX XX'},
                        { url: 'http://cnn.com/', text: "XXXXXXXXX"},
                        { url: 'http://cnn.com/', text: 'XXXXXXXXX XXX'}
                    ]
                ]
            },
            {
                title: "YYYYYYYY YYYYY",
                cls: 'home-green',
                links: [
                    [
                        { subsection: 'XXX XXXXXXX'},
                        { html: '<p>Some text goes here</p>' },
                        { subsection: 'XXXXX  XXXXXXXXXX'},
                        { html: '<p>Some text goes here<br>' +
                            'Some text goes here<br>' +
                            'Some text goes here<br>' +
                            'Some text goes here</p>' }
                    ]
                ]
            }
        ];

		return {
			data: resources
		};
	}

	//--------------

	angular.module('home')
		.factory('HomeDataService', HomeDataService);

})();